package kz.astana.layoutapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private GridLayout gl;
    private LinearLayout ll;
    private RelativeLayout rl;
    private LinearLayout lll;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gl = findViewById(R.id.gridLayout);
        ll = findViewById(R.id.linearLayout);
        rl = findViewById(R.id.relativeLayoutLinear);
        lll = findViewById(R.id.linearLayoutLinear);

        Button one = findViewById(R.id.one);
        one.setOnClickListener(this);
        Button two = findViewById(R.id.two);
        two.setOnClickListener(this);
        Button three = findViewById(R.id.three);
        three.setOnClickListener(this);
        Button four = findViewById(R.id.four);
        four.setOnClickListener(this);
        Button five = findViewById(R.id.five);
        five.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.one) {
            gl.setVisibility(View.VISIBLE);
            ll.setVisibility(View.GONE);
        } else if (v.getId() == R.id.two) {
            gl.setVisibility(View.GONE);
            ll.setVisibility(View.VISIBLE);
            rl.setVisibility(View.VISIBLE);
            lll.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.three) {
            rl.setVisibility(View.VISIBLE);
            lll.setVisibility(View.INVISIBLE);
        } else if (v.getId() == R.id.four) {
            rl.setVisibility(View.INVISIBLE);
            lll.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.five) {
            Intent i = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(i);
        }
    }
}
